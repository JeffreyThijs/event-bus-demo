package io.aloxy

import io.quarkus.runtime.StartupEvent
import io.quarkus.vertx.ConsumeEvent
import io.quarkus.vertx.LocalEventBusCodec
import io.smallrye.mutiny.Uni
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.mutiny.core.eventbus.EventBus
import io.vertx.mutiny.core.eventbus.Message
import org.jboss.logging.Logger
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.Observes


@ApplicationScoped
class Producer(eventBus: EventBus) {
    private val codec = LocalEventBusCodec<MessagePayload>()
    init {
        eventBus.registerCodec(codec)
        val handler = { message: Message<MessagePayload> ->
            val options = DeliveryOptions().apply {
                codecName = codec.name()
            }
            message.reply(consume(message.body()), options)
        }
        eventBus.consumer<MessagePayload>("event")
            .handler(handler)
    }

    fun consume(message: MessagePayload): MessagePayload {
        return MessagePayload("received ${message.payload}")
    }

    fun onStart(@Observes ev: StartupEvent) {}
}