package io.aloxy

import io.quarkus.vertx.LocalEventBusCodec
import io.smallrye.mutiny.Uni
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.mutiny.core.eventbus.EventBus
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@ApplicationScoped
@Path("/message")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
class Consumer(
    private val eventBus: EventBus
) {
    private val codec = LocalEventBusCodec<MessagePayload>()
    init {
        eventBus.registerCodec(codec)
    }
    @POST
    fun post(messagePayload: MessagePayload): Uni<MessagePayload> {
        val options = DeliveryOptions().apply {
            codecName = codec.name()
        }
        return eventBus.request<MessagePayload>("event", messagePayload, options)
            .map { it.body() }
    }
}