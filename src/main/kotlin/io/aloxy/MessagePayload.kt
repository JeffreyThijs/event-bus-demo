package io.aloxy

import com.fasterxml.jackson.annotation.JsonProperty

data class MessagePayload(
    @field:JsonProperty("payload")
    val payload: String
)
